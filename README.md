# 微信小程序测量计算工具

>熟悉微信小程序界面模块开发

![image](https://github.com/tujindong/survey-tools/blob/master/dist/images/page1.jpg)

## 小功能
* 计算三角形面积
* 计算多边形面积
* 度分秒转角度
* 弧度转角度
* 距离交会
* 角度交会
* 坐标正算
* 坐标反算
* 无往返支水准路线
* 简单小计算器

## 扫描下方小程序码体验

![image](https://gitee.com/cloud_motion/drawing-bed/raw/master/drawing-bed/2022-4-1/%E5%BE%AE%E4%BF%A1%E5%9B%BE%E7%89%87_20220401201802-1648815500397.jpg)
