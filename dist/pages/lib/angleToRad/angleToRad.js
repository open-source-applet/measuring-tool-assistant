// pages/lib/angleToRad/angleToRad.js
//角度转换成十进制数字格式
var angleToNum = function (deg, min, sec) {
  var num = parseFloat(deg) + (parseFloat(min) / 60) + (parseFloat(sec) / 3600);
  return num;
}
//角度转换成弧度
var angleToRad = function (deg, min, sec) {
  var num = angleToNum(deg, min, sec);
  var result = num * Math.PI / 180;
  return result
}
Page({
  data: {
    value:'',
    result:0,
    d:'',
    m:'',
    s:''
  },
  inputD:function(e){
    var n = e.detail.value;
    this.setData({
      d:n
    })
  },
  inputM:function(e){
    var n = e.detail.value;
    this.setData({
      m:n
    })
  },
  inputS:function(e){
    var n = e.detail.value;
    this.setData({
      s:n
    })

  },
  angleToRad:function(e){
    var d = this.data.d;
    var m = this.data.m;
    var s = this.data.s;
    if(!d.length == 0 && !m.length == 0 && !s.length == 0){
      if(!isNaN(d) && !isNaN(m) && !isNaN(s)){
        var result = angleToRad(parseFloat(d), parseFloat(m), parseFloat(s)).toFixed(4) + ' rad';
        this.setData({
          result: result
        })
      }else{
        wx.showToast({
          title: '乖！请填数字',
          icon:'none',
          duration:1500
        })
      }
    }else{
      wx.showToast({
        title: '小马虎！请填完整',
        icon:'none',
        duration:1500
      })
    }
  },
  clear:function(e){
    this.setData({
      value:'',
      result:0
    })
  }
})