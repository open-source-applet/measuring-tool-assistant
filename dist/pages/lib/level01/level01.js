// pages/lib/level01/level01.js
Page({
  data: {
    activeIndex: 0,
    content: 'BM.A',
    tabs: [
      {
        id: 1,
        tabName: 'BM.A',
        title: ['已知高程：', '后视读数1：', '后视读数2：'],
        unit:['单位m','单位mm','单位mm']
      },
    ],
    val0:'',
    val1:'',
    val2:'',
    val3:'',
    value: '',
    lists: [],
    num_lists:0,
    num:0,
    flag:true,
    arrDisLevel:[],
    arrLevel:[]
  },
  //选择当前点
  changeTab: function (e) {
    this.setData({
      activeIndex: e.currentTarget.dataset.index,
      content: e.currentTarget.dataset.name,
    })
    var val0, val1, val2, val3, lists, activeIndex;
    activeIndex = this.data.activeIndex;
    lists = this.data.lists;
    val0 = lists[activeIndex].val0;
    val1 = lists[activeIndex].val1;
    val2 = lists[activeIndex].val2;
    val3 = lists[activeIndex].val3;
    this.setData({
      val0:val0,
      val1:val1,
      val2:val2,
      val3:val3
    })
  },
  //添加测站点
  addDot:function(e){
    var lists = this.data.lists;
    var tabs = this.data.tabs;
    var activeIndex = this.data.activeIndex;
    var content = this.data.content;
    for(var i=0; i<tabs.length; i++){
      var id = i+2;
    }
    if(tabs.length == lists.length){
      //添加测站点名称
      var tabObj = {
        id: id,
        tabName: 'TP' + (id - 1),
        title: ['前视读数1：', '前视读数2：', '后视读数1：', '后视读数2：'],
        unit:['单位mm','单位mm','单位mm','单位mm']
      };
      tabs.push(tabObj);
      this.setData({
        tabs: tabs,
        activeIndex: id - 1,
        value: ''
      })
    }else{
      wx.showToast({
        title: '请确定测站点',
        icon:'none',
        duration:1500
      })
    }
  },
  //删除测站点
  delDot:function(e){
    var tabs = this.data.tabs;
    var activeIndex = this.data.activeIndex;
    var lists = this.data.lists;
    var num_lists = this.data.num_lists;
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确定删除TP'+ activeIndex +'测站点？',
      success:function(res){
        if(res.confirm){
          tabs.splice(activeIndex, 1);
          lists.splice(activeIndex, 1);
          var len = lists.length;
          that.setData({
            tabs: tabs,
            activeIndex: activeIndex - 1,
            lists:lists,
            num_lists:len
          })
        }
      }
    })
  },
  //测站点数据输入绑定
  inputVal0:function(e){
    var n = e.detail.value;
    var val0 = this.data.lists.val0;
    this.setData({
      val0:n
    })
  },
  inputVal1:function(e){
    var n = e.detail.value;
    var val1 = this.data.lists.val1;
    this.setData({
      val1:n
    })
  },
  inputVal2:function(e){
    var n = e.detail.value;
    var val2 = this.data.lists.val2;
    this.setData({
      val2:n
    })
  },
  inputVal3:function(e){
    var n = e.detail.value;
    var val3 = this.data.lists.val3;
    this.setData({
      val3:n
    })
  },
  //确定测站点
  confirmDot:function(e){
    var activeIndex = this.data.activeIndex;
    var lists = this.data.lists;
    var tabs = this.data.tabs;
    var val0, val1, val2, val3, num_lists;
    val0 = this.data.val0;
    val1 = this.data.val1;
    val2 = this.data.val2;
    val3 = this.data.val3;
    num_lists = this.data.num_lists;
    //输入内容判断
    if (!val0.length == 0 && !val1.length == 0 && !val2.length == 0 && (activeIndex == 0?true:!val3.length == 0)){
      if (!isNaN(val0) && !isNaN(val1) && !isNaN(val2) && (activeIndex == 0?true:!isNaN(val3))){
        if (lists.length < tabs.length) { //防止重复提交
          lists.push({ val0, val1, val2, val3 })
          wx.showToast({
            title: '测站点' + tabs[activeIndex].tabName + '已录入',
            icon: 'none',
            duration: 1500
          })
        } else {
          wx.showToast({
            title: '已确定',
            icon: 'none',
            duration: 1500
          })
        }
        if (lists.length == tabs.length) {  //修改提交表单
          lists.splice(activeIndex, 1, { val0, val1, val2, val3 })
          wx.showToast({
            title: '测站点' + tabs[activeIndex].tabName + '已录入',
            icon: 'none',
            duration: 1500
          })
        }
        var len = lists.length;
        this.setData({
          lists: lists,
          num_lists: len
        })
      }else{
        wx.showToast({
          title: '乖！请填数字',
          icon: 'none',
          duration: 1500
        })
      }
    }else{
      wx.showToast({
        title: '小马虎！请输入完整',
        icon: 'none',
        duration: 1500
      })
    }
  },
  //计算测站点
  calcLevel:function(e){
    var lists = this.data.lists;
    var num = this.data.num;
    num = lists.length;
    //测站点高差
    var arrVal0 = [], arrVal1 = [], arrVal2 = [], arrVal3 = [], _v0, _v1, _v2, _v3;
    for(var i = 0; i<lists.length; i++){
      arrVal0.push(lists[i].val0);
      arrVal1.push(lists[i].val1);
      arrVal2.push(lists[i].val2);
      arrVal3.push(lists[i].val3);
    }
    _v0 = arrVal0.map(Number);  
    _v1 = arrVal1.map(Number);
    _v2 = arrVal2.map(Number);
    _v3 = arrVal3.map(Number);
    var arrDisLevel = [];  // //TP测站点高差
    var arrLevel = []; //测站点高程
    var mba = _v0[0]; //已知点 MB.A的高程 单位m
    for(var i = 1; i < _v0.length; i++){
      var disLevel = ((_v0[i] + _v1[i]) - (_v2[i] + _v3[i]))/2; 
      arrDisLevel.push(disLevel);
    }
    arrDisLevel.unshift(0);
    for(var i = 0; i < arrDisLevel.length; i++){
      var level = arrDisLevel[i] / 1000 + mba;
      arrLevel.push(level);
    }
    this.setData({
      arrDisLevel:arrDisLevel,
      arrLevel: arrLevel
    })
  },
  //清空所有测站点
  clear:function(e){
    var tabs = this.data.tabs;
    var lists = this.data.lists;
    var num_lists = this.data.num_lists;
    var activeIndex = this.data.activeIndex;
    var arrDisLevel = this.data.arrDisLevel;
    var arrLevel = this.data.arrLevel;
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确定清空所有测站点数据？',
      success:function(res){
        if(res.confirm){
          tabs.splice(1, tabs.length);
          lists.splice(0, lists.length);
          arrDisLevel.splice(0, arrDisLevel.length);
          arrLevel.splice(0, arrLevel.length);
          that.setData({
            value: '',
            tabs: tabs,
            lists:lists,
            arrDisLevel:arrDisLevel,
            arrLevel: arrLevel,
            num_lists: 0,
            activeIndex: 0
          })
        }
      }
    })
  },
})