// pages/lib/cityToBuilt/cityToBuilt.js
//角度转换成十进制数字格式
var angleToNum = function (deg, min, sec) {
  var num = parseFloat(deg) + (parseFloat(min) / 60) + (parseFloat(sec) / 3600);
  return num;
}
//角度转换成弧度
var angleToRad = function (deg, min, sec) {
  var num = angleToNum(deg, min, sec);
  var result = num * Math.PI / 180;
  return result
}
Page({
  data: {
    xo:'',
    yo:'',
    x_p:'',
    y_p:'',
    angle:'',
    xp: 0,
    yp: 0,
    value:''
  },
  inputXo:function(e){
    var n = e.detail.value;
    this.setData({
      xo:n
    })
  },
  inputYo:function(e){
    var n = e.detail.value;
    this.setData({
      yo:n
    })
  },
  inputX_p:function(e){
    var n = e.detail.value;
    this.setData({
      x_p:n
    })
  },
  inputY_p:function(e){
    var n = e.detail.value;
    this.setData({
      y_p:n
    })
  },
  inputAngle:function(e){
    var n = e.detail.value;
    this.setData({
      angle:n
    })
  },
  cityToBuilt:function(e){
    var xo, yo, x_p, y_p, angle, arrAngle, radAngle, xp, yp;
    xo = this.data.xo;
    yo = this.data.yo;
    x_p = this.data.x_p;
    y_p = this.data.y_p;
    angle = this.data.angle;
    arrAngle = [];
    arrAngle = angle.trim().split(/[ |,|，]/);
    radAngle = angleToRad(arrAngle[0], arrAngle[1], arrAngle[2]);
    if(!xo.length==0 && !yo.length==0 && !x_p.length==0 && !y_p.length==0 && !angle.length==0){
      if(!isNaN(xo) && !isNaN(yo) && !isNaN(x_p) && !isNaN(y_p)){
        for (var i = 0; i < arrAngle.length; i++) {
          if (isNaN(arrAngle[i])) {
            wx.showToast({
              title: '乖！请填数字',
              icon: 'none',
              duration: 1500
            })
            return
          }
        };
        if (arrAngle.length !== 3) {
          wx.showToast({
            title: '请检查角度输入格式是否有误。可以参考示例：30 30 30',
            icon: 'none',
            duration: 2000
          })
          return
        }
        //建筑坐标系转城市坐标系
        var _xo = parseFloat(xo);
        var _yo = parseFloat(yo);
        var _x_p = parseFloat(x_p);
        var _y_p = parseFloat(y_p);
        xp = (_xo + _x_p * Math.cos(radAngle) - _y_p * Math.sin(radAngle)).toFixed(4);
        yp = (_yo + _x_p * Math.sin(radAngle) + _y_p * Math.cos(radAngle)).toFixed(4);
        this.setData({
          xp: xp,
          yp: yp
        })
      }else{
        wx.showToast({
          title: '乖！请填数字',
          icon: 'none',
          duration: 1500
        })
      }
    }else{
      wx.showToast({
        title: '小马虎！请填完整',
        icon: 'none',
        duration: 1500
      })
    }
  },
  clear:function(e){
    this.setData({
      value: '',
      xp: 0,
      yp: 0
    })
  },
})