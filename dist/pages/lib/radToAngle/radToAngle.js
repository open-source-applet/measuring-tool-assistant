// pages/lib/radToAngle/radToAngle.js
//弧度转角度，并以度分秒格式输出
var radToAngle = function (rad) {
  var num = rad * 180 / (Math.PI);
  var deg = parseInt(num);
  var min = parseInt((num - deg) * 60);
  var sec = parseFloat(((num - deg) * 60 - min) * 60).toFixed(2);
  return deg + ' 度 ' + min + " 分 " + sec + ' 秒 ';
}
Page({
  data: {
    rad:'',
    value:'',
    result:0
  },
  inputRad:function(e){
    var n = e.detail.value;
    this.setData({
      rad:n
    })
  },
  radToAngle:function(e){
    var rad = this.data.rad;
    if(!rad.length == 0){
      if(!isNaN(rad)){
        var result = radToAngle(parseFloat(rad));
        this.setData({
          result: result
        })
      }else{
        wx.showToast({
          title: '乖！请填数字',
          icon: 'none',
          duration: 1500
        })
      }
    }else{
      wx.showToast({
        title: '小马虎！请填完整',
        icon:'none',
        duration:1500
      })
    }
  },
  clear:function(e){
    this.setData({
      value:'',
      result:0
    })
  }
})