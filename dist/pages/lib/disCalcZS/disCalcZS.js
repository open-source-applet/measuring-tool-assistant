// pages/lib/disCalcZS/disCalcZS.js
//角度转换成十进制数字格式
var angleToNum = function (deg, min, sec) {
  var num = parseFloat(deg) + (parseFloat(min) / 60) + (parseFloat(sec) / 3600);
  return num;
}
//角度转换成弧度
var angleToRad = function (deg, min, sec) {
  var num = angleToNum(deg, min, sec);
  var result = num * Math.PI / 180;
  return result
}
Page({
  data: {
    xa:'',
    ya:'',
    ab:'',
    angle:'',
    value:'',
    xb:0,
    yb:0
  },
  inputXa:function(e){
    var n = e.detail.value;
    this.setData({
      xa:n
    })
  },
  inputYa:function(e){
    var n = e.detail.value;
    this.setData({
      ya:n
    })
  },
  inputAB:function(e){
    var n = e.detail.value;
    this.setData({
      ab:n
    })
  },
  inputAngle:function(e){
    var n = e.detail.value;
    this.setData({
      angle:n
    })
  },
  disCalc:function(e){
    var xa, ya, ab, angle, xb, yb, arrAngle = [], radAngle;
    xa = this.data.xa;
    ya = this.data.ya;
    ab = this.data.ab;
    angle = this.data.angle;
    arrAngle = angle.trim().split(/[ |,|，]/);
    radAngle = angleToRad(arrAngle[0], arrAngle[1], arrAngle[2]);
    if(!xa.length==0 && !ya.length==0 && !ab.length==0 && !angle.length==0){
      if(!isNaN(xa) && !isNaN(ya) && !isNaN(ab)){
        for (var i = 0; i < arrAngle.length; i++) {
          if (isNaN(arrAngle[i])) {
            wx.showToast({
              title: '乖！请填数字',
              icon: 'none',
              duration: 1500
            })
            return
          }
        };
        if(arrAngle.length!==3){
          wx.showToast({
            title: '请检查角度输入格式是否有误。可以参考示例：30 30 30',
            icon: 'none',
            duration: 2000
          })
          return
        }
        //坐标正算计算
        var _xa, _ya, _ab
        _xa = parseFloat(xa);
        _ya = parseFloat(ya);
        _ab = parseFloat(ab);
        xb = (_xa + _ab * Math.cos(radAngle)).toFixed(4);
        yb = _ya + _ab * Math.sin(radAngle).toFixed(4);
        this.setData({
          xb: xb,
          yb: yb
        })
      }else{
        wx.showToast({
          title: '乖！请填数字',
          icon: 'none',
          duration: 1500
        })
      }
    }else{
      wx.showToast({
        title: '小马虎！请填完整',
        icon:'none',
        duration:1500
      })
    }
  },
  clear:function(e){
    this.setData({
      value:'',
      xb:0,
      yb:0
    })
  },
})