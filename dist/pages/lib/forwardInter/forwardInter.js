// pages/lib/forwardInter/forwardInter.js
//角度转换成十进制数字格式
var angleToNum = function (deg, min, sec) {
  var num = parseFloat(deg) + (parseFloat(min) / 60) + (parseFloat(sec) / 3600);
  return num;
}
//角度转换成弧度
var angleToRad = function (deg, min, sec) {
  var num = angleToNum(deg, min, sec);
  var result = num * Math.PI / 180;
  return result
}
//计算P点的x坐标
var calcXp = function(angleA, xa, ya, angleB, xb, yb, sign){
  var _a, _b, _c, _d, _f
  _a = xa * Math.sin(angleA) * Math.cos(angleB);
  _b = xb * Math.cos(angleA) * Math.sin(angleB);
  _c = sign * (ya - yb) * Math.sin(angleA) * Math.sin(angleB);
  _d = Math.sin(angleA) * Math.cos(angleB);
  _f = Math.cos(angleA) * Math.sin(angleB);
  if((_d + _f) == 0){
    wx.showToast({
      title: '请检查数字',
      icon:'none',
      duration:1500
    })
  }else{
    var result = (_a + _b + _c)/(_d + _f);
    return result;
  }
}
//计算P点y坐标
var calcYp = function(angleA, xa, ya, angleB, xb, yb, sign){
  var _a, _b, _c, _d, _f;
  _a = ya * Math.sin(angleA) * Math.cos(angleB);
  _b = yb * Math.cos(angleA) * Math.sin(angleB);
  _c = sign * (xb - xa) * Math.sin(angleA) * Math.sin(angleB);
  _d = Math.sin(angleA) * Math.cos(angleB);
  _f = Math.cos(angleA) * Math.sin(angleB);
  if((_d + _f) == 0){
    wx.showToast({
      title: '两点重合或在一条直线上',
      icon:'none',
      duration:1500
    })
  }else{
    var result = (_a + _b + _c)/(_d + _f);
    return result;
  }
}
Page({
  data: {
    checked:false,
    value:'',
    xa: '',
    ya: '',
    xb: '',
    yb: '',
    angleA: '',
    angleB: '',
    sign: '',
    xp: 0,
    yp: 0
  },
  inputXa:function(e){
    var n = e.detail.value;
    this.setData({
      xa: n
    })
  },
  inputYa:function(e){
    var n = e.detail.value;
    this.setData({
      ya: n
    })
  },
  inputXb:function(e){
    var n = e.detail.value;
    this.setData({
      xb: n
    })
  },
  inputYb:function(e){
    var n = e.detail.value;
    this.setData({
      yb: n
    })
  },
  inputAngleA:function(e){
    var n = e.detail.value;
    this.setData({
      angleA: n
    })
  },
  inputAngleB:function(e){
    var n = e.detail.value;
    this.setData({
      angleB: n
    })
  },
  radioChange:function(e){
    var n = e.detail.value;
    this.setData({
      sign: n
    })
  },
  forwardInter:function(e){
    var xa = this.data.xa;
    var ya = this.data.ya;
    var xb = this.data.xb;
    var yb = this.data.yb;
    var angleA = this.data.angleA;
    var angleB = this.data.angleB;
    var sign = this.data.sign;
    if(!xa.length==0 && !ya.length==0 && !xb.length==0 && !yb.length==0 && !angleA.length==0 && !angleB.length==0 && !sign.length==0){
       if(!isNaN(xa) && !isNaN(ya) && !isNaN(xb) && !isNaN(yb)){
        //计算前方交会
        var arrAngleA = [], arrAngleB = [], radA, radB;
        arrAngleA = angleA.trim().split(/[ |,|，]/);
        radA = angleToRad(arrAngleA[0], arrAngleA[1], arrAngleA[2]);
        arrAngleB = angleB.trim().split(/[ |,|，]/);
        radB = angleToRad(arrAngleB[0], arrAngleB[1], arrAngleB[2]);
        for(var i=0; i<arrAngleA.length; i++){
          if(isNaN(arrAngleA[i])){
            wx.showToast({
              title: '乖！请填数字',
              icon:'none',
              duration:1500
            })
            return
          }
        };
        for(var i=0; i<arrAngleB.length; i++){
          if(isNaN(arrAngleB[i])){
            wx.showToast({
              title: '乖！请填数字',
              icon:'none',
              duration:1500
            })
            return
          }
        };
         if (!arrAngleA.length == 3 && !arrAngleB.length==3) {
           wx.showToast({
             title: '请检查角度输入格式是否有误。可以参考示例：30 30 30',
             icon: 'none',
             duration: 2000
           })
           return
         }
        var _xp = calcXp(radA, parseFloat(xa), parseFloat(ya), radB, parseFloat(xb), parseFloat(yb), parseFloat(sign));
        var _yp = calcYp(radA, parseFloat(xa), parseFloat(ya), radB, parseFloat(xb), parseFloat(yb), parseFloat(sign));
        var xp = _xp.toFixed(5);
        var yp = _yp.toFixed(5);
        this.setData({
          xp: xp,
          yp: yp
        })
       }else{
         wx.showToast({
           title: '乖，请填数字',
           icon: 'none',
           duration: 1500
         })
       }
    }else{
      wx.showToast({
        title: '小马虎，请填完整',
        icon: 'none',
        duration: 1500
      })
    }
  },
  clear:function(e){
    this.setData({
      value: '',
      checked:false,
      xp: 0,
      yp: 0
    })
  },
})