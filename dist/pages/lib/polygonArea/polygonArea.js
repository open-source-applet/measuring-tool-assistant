// pages/lib/polygonArea/polygonArea.js
Page({
  data: {
    list:[],
    selList:[],
    value:'',
    inputx:'',
    inputy:'',
    result: 0,
    id:0,
    iconStatu:false
  },
  onLoad:function(options){
    var dataList = this.data.list;
    dataList.map(function(value){
      value.selStatu = false;
    })
  },

  //获取输入框数组
  inputX:function(e){
    var n = e.detail.value;
    this.setData({
      inputx:n
    })  
  },
  inputY:function(e){
    var n = e.detail.value;
    this.setData({
      inputy:n
    })
  },
  //添加点
  addDot: function (e) {
    var id = this.data.id;
    var inputx = this.data.inputx;
    var inputy = this.data.inputy;
    var list = this.data.list;
    for(var i = 0; i<list.length; i++){
      id = i+1;
    }
    var obj = {
      detail:'X: ' + inputx +' ; ' + 'Y: ' + inputy,
      id:id
    }
    if(!inputx.length == 0 && !inputy.length == 0){
      if(!isNaN(inputx) && !isNaN(inputy)){
        list.push(obj);  //添加列表
        this.setData({
          id: id,
          list: list,
          value: '',
          inputx: '',
          inputy: ''
        })
      }else{
        wx.showToast({
          title: '乖！请填数字',
          icon: 'none',
          duration: 1500
        })
      }
    }else{
      wx.showToast({
        title: '小马虎！请填入完整',
        icon:'none',
        duration:1500
      })
    }
   },
  //选择框出现或取消
  showSelIcon:function(e){
    this.setData({
      iconStatu: !this.data.iconStatu
    })
  },
  //选中点
  toggleSel:function(e){
    if(this.data.iconStatu){
      var selArr = this.data.selList;
      var selId = e.target.dataset.id || e.currentTarget.dataset.id;
      var dataList = this.data.list;
      var index = this.data.selList.indexOf(selId);
      if(index < 0){
        selArr.push(e.target.dataset.id);
        dataList.map((value)=>{
          if(value.id == selId){
            value.selStatu = true
          }
        })
      }else{
        dataList.map((value) =>{
          if(value.id == selId){
            value.selStatu = false
          }
        })
        selArr.splice(index,1)
      }
      this.setData({
        selList:selArr,
        list:dataList
      })
    }
  },
  //删除点
  delItem:function(e){
    var that = this;
    wx.showModal({
      title: '提示',
      content:'确定删除点？',
      success:function(res){
        if(res.confirm){
          var arr = that.data.list;
          var selArr = that.data.selList;
          for (var i = 0; i < selArr.length; i++) {
            arr = arr.filter((value, index) => {
              return value.id != selArr[i]
            })
          }
          for (var i = 0; i < arr.length; i++) {
            arr[i].selStatu = false
          }
          that.setData({
            list: arr,
            selList: [],
            iconStatu:false
          })
        }
      }
    })
  },
  //计算面积
  calcArea:function(e){
    var lists = this.data.list;
    var arrDetail = [], arrX = [], arrY = [];
    for(var i=0; i<lists.length; i++){
      arrDetail = lists[i].detail.split(';');
      arrX.push(arrDetail[0].split(':')[1]);
      arrY.push(arrDetail[1].split(':')[1]);
    }
    if (lists.length > 2) {
      var sum0 = 0;
      // var arrX = [0, 4, 4, 0];
      // var arrY = [0, 0, 3, 3];
      //多边形面积计算
      for(var i=0; i < arrX.length - 1; i++){
        sum0 = sum0 + (arrX[i] * arrY[i + 1] - arrX[i + 1] * arrY[i]);
      }
      var area = (Math.abs(sum0 + (arrX[i] * arrY[0]) - (arrX[0] * arrY[i]))) / 2;
      this.setData({
        result:area
      })
    } else {
      wx.showToast({
        title: '至少输入三个点',
        icon:'none',
        duration:1500
      })
    }
  },
  clear:function(e){
    this.setData({
      value: '',
      result: 0,
      num:0,
      inputx:'',
      inputy:'',
      list:[]
    })
  }
})