// pages/tools/tools.js
Page({
  data: {
    items:[
      {
        title: '计算三角形面积',
        bindtap:'triangleArea'
      },
      {
        title: '计算多边形面积',
        bindtap: 'polygonArea'
      },
      {
        title: '无往返支水准路线',
        bindtap: 'level01'
      },
      {
        title: '距离交会',
        bindtap: 'disInter'
      },
      {
        title: '角度前方交会',
        bindtap: 'forwardInter'
      },
      {
        title: '坐标正算',
        bindtap: 'disCalcZS'
      },
      {
        title: '坐标反算',
        bindtap: 'disCalcFS'
      },
      {
        title: '城市坐标系 → 建筑坐标系',
        bindtap: 'cityToBuilt'
      },
      {
        title: '度分秒 → 角度',
        bindtap: 'dmsToDeg'
      },
      {
        title: '角度 → 度分秒',
        bindtap: 'degToDms'
      },
      {
        title: '角度 → 弧度',
        bindtap: 'angleToRad'
      },
      {
        title: '弧度 → 角度',
        bindtap: 'radToAngle'
      },
      {
        title: '地图(获取当前位置)',
        bindtap: 'location'
      },
      {
        title: '计算器',
        bindtap: 'calcMech'
      },
    ]
  },
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
    }
  },
  triangleArea:function(e){
    wx.navigateTo({
      url:'../lib/triangleArea/triangleArea'
    })
  },
  disCalcFS:function(e){
    wx.navigateTo({
      url: '../lib/disCalcFS/disCalcFS'
    })
  },
  disCalcZS:function(e){
    wx.navigateTo({
      url: '../lib/disCalcZS/disCalcZS'
    })
  },
  disInter:function(e){
    wx.navigateTo({
      url:'../lib/disInter/disInter'
    })
  },
  forwardInter:function(e){
    wx.navigateTo({
      url:'../lib/forwardInter/forwardInter'
    })
  },
  polygonArea:function(e){
    wx.navigateTo({
      url: '../lib/polygonArea/polygonArea'
    })
  },
  dmsToDeg:function(e){
    wx.navigateTo({
      url: '../lib/dmsToDeg/dmsToDeg'
    })
  },
  degToDms:function(e){
    wx.navigateTo({
      url: '../lib/degToDms/degToDms'
    })
  },
  angleToRad:function(e){
    wx.navigateTo({
      url: '../lib/angleToRad/angleToRad'
    })
  },
  radToAngle:function(e){
    wx.navigateTo({
      url: '../lib/radToAngle/radToAngle'
    })
  },
  cityToBuilt:function(e){
    wx.navigateTo({
      url: '../lib/cityToBuilt/cityToBuilt'
    })
  },
  level01: function (e) {
    wx.navigateTo({
      url: '../lib/level01/level01'
    })
  },
  location: function(e){
    wx.navigateTo({
      url: '../lib/location/location'
    })
  },
  calcMech:function(e){
    wx.navigateTo({
      url: '../lib/calcMech/calcMech'
    })
  }
})