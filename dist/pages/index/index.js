'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _configJson = require('./config.json.js');

exports.default = Page({
  data: {
    '__code__': {
      readme: '',
    },
    menus: _configJson.menus,
    bannerList:[
      '../../images/banner_01.png',
      '../../images/banner_02.png',
      '../../images/banner_03.png'
    ],
    itemLists1: [
      {
        id:1,
        imgSrc: '../../images/item_01.png',
        desc: '计算多边形面积'
      },
      {
        id:2,
        imgSrc: '../../images/item_02.png',
        desc: '无往返支水准路线'
      },
      {
        id: 3,
        imgSrc: '../../images/item_03.png',
        desc: '城市建筑坐标系转换'
      },
      {
        id: 4,
        imgSrc: '../../images/item_04.png',
        desc: '角度前方交会'
      },
      {
        id: 5,
        imgSrc: '../../images/item_05.png',
        desc: '坐标正算'
      },
      {
        id: 6,
        imgSrc: '../../images/item_06.png',
        desc: '坐标反算'
      },
    ],
    itemLists2: [
      {
        id: 1,
        imgSrc: '../../images/item_07.png',
        desc: '简单计算器'
      },
      {
        id: 2,
        imgSrc: '../../images/item_08.png',
        desc: '获取位置'
      },
      {
        id: 3,
        imgSrc: '../../images/item_09.png',
        desc: '支导线坐标计算'
      },
      {
        id: 4,
        imgSrc: '../../images/item_10.png',
        desc: '更多'
      },
    ]
  },
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
    }
  },
  navToSel:function(e){
    var activeIndexSel = e.currentTarget.dataset.index || e.target.dataset.index;
    switch(activeIndexSel){
      case 1:
        wx.navigateTo({
          url: '../lib/polygonArea/polygonArea',
        })
        break;
      case 2:
        wx.navigateTo({
          url: '../lib/level01/level01',
        })
        break;
      case 3:
        wx.navigateTo({
          url: '../lib/cityToBuilt/cityToBuilt',
        })
        break;
      case 4:
        wx.navigateTo({
          url: '../lib/forwardInter/forwardInter',
        })
        break;
      case 5:
        wx.navigateTo({
          url: '../lib/disCalcZS/disCalcZS',
        })
        break;
      case 6:
        wx.navigateTo({
          url: '../lib/disCalcFS/disCalcFS',
        })
        break;
      default:
        break;
    }
  },
  navToMore:function(e){
    var activeIndexMore = e.currentTarget.dataset.index || e.target.dataset.index;
    switch(activeIndexMore){
      case 1:
        wx.navigateTo({
          url: '../lib/calcMech/calcMech',
        })
        break;
      case 2:
        wx.navigateTo({
          url: '../lib/location/location',
        })
        break;
      case 3:
        wx.showToast({
          title: '弥补测量知识中，导线计算onReading...',
          icon:'none',
          duration:1500
        })
        break;
      case 4:
        wx.showToast({
          title: '期待更多功能...',
          icon: 'none',
          duration: 1500
        })
        break;
      default:
        break;
    }
  }
});
