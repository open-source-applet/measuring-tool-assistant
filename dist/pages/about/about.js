// pages/about/about.js
Page({
  data: {
    activeIndex:0,
    detailFlag:true,
    oneWordIndex:0,
    num:0,
    lists:[
      {
        id:1,
        title:'一句话',
        label_title:'label_title1'
      },
      {
        id:2,
        title: '发现更多',
        label_title: 'label_title2'
      },
      {
        id:3,
        title: '联系我',
        label_title: 'label_title3'
      }
    ],
    oneWord:[
      {
        id:1,
        text: '看不到棱镜，同学你点下子屏幕撒!...'
      },
      {
        id: 2,
        text: '老大，是不是该过去把那树坎了，咱的镜头被挡了!'
      },
      {
        id: 3,
        text: '4687和4787啥子区别?'
      },
      {
        id: 4,
        text: '艹 ....水准气泡都没进圈你就测!'
      },
      {
        id: 5,
        text: ' 早点出发，凉快，测完早点收工。'
      },
      {
        id: 6,
        text: '这个点超限没？没超的话搬站棱镜朝我这边转转!'
      },
      {
        id: 7,
        text: '孩子们，站好了，哥哥给你们照相。。'
      },
      {
        id: 8,
        text: '尺子立正终于快画完了。我靠。电脑重启了。妈的忘保存了...'
      },
      {
        id: 9,
        text: '再做个DEM，平三角都给我去了哎。。'
      },
      {
        id: 10,
        text: '大哥，几十万的东西，别碰！'
      },
      {
        id: 11,
        text: '快点，马上11点了，没固定解!~'
      },
      {
        id: 12,
        text: '狗狗狗!!!!!快跑!~~~'
      },
      {
        id: 13,
        text: '麻蛋，刚拆了仪器，发现还有一个重要的点没测。。'
      },
      {
        id: 14,
        text: '我跟你说了多少遍了~不要坐在仪器箱上玩手机。'
      },
      {
        id: 15,
        text: '我擦!控制点的桩子叫哪个王八羔子给拔了?！'
      },
      {
        id: 16,
        text: '以上纯属娱乐。最后，致敬每一个坚守在测绘岗位上的人...辛苦了。'
      },
    ]
  },
  onReady:function(){
    //页面渲染完成
  },
  showDetail:function(e){
    var activeIndex = e.currentTarget.dataset.id || e.target.dataset.id;
    if (activeIndex==1){
      wx.navigateTo({
        url: '/pages/find/find',
      });return;
    }
    var detailFlag = this.data.detailFlag;
    var oneWordIndex = this.data.oneWordIndex;
    var num = this.data.num;
    this.setData({
      activeIndex: activeIndex, 
      detailFlag : false,
      oneWordIndex:0,
      num:0
    })
  },
  hideDetail:function(e){
    var detailFlag = this.data.detailFlag;
    var oneWordIndex = this.data.oneWordIndex;
    var num = this.data.num;
    this.setData({
      detailFlag: true,
      oneWordIndex: 0,
      num:0
    })
  },
  oneWordNext:function(e){
    var oneWord = this.data.oneWord;
    var oneWordIndex = e.currentTarget.dataset.index || e.target.dataset.index;
    var num = this.data.num;
    num<15 ? num++ : num=15;
    oneWordIndex = num;
    this.setData({
      num:num,
      oneWordIndex: oneWordIndex
    })
  }
})